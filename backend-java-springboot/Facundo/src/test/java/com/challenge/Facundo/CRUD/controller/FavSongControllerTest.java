package com.challenge.Facundo.CRUD.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.challenge.Facundo.CRUD.dto.FavSongDto;
import com.challenge.Facundo.CRUD.entity.FavSong;
import com.challenge.Facundo.CRUD.repository.FavSongRepository;
import com.challenge.Facundo.CRUD.service.FavSongService;
import com.challenge.Facundo.GLOBAL.dto.MessageDto;
import com.challenge.Facundo.GLOBAL.exceptions.ResourceNotFoundException;
import com.challenge.Facundo.SECURITY.entity.Usuario;
import com.challenge.Facundo.SECURITY.repository.UsuarioRepository;
import com.challenge.Facundo.SECURITY.service.UsuarioService;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

class FavSongControllerTest {
    /**
     * Method under test: {@link FavSongController#listAll(Authentication)}
     */
    @Test
    void testListAll() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.findAllByUsuarioId(anyInt())).thenReturn(new ArrayList<>());
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongService favSongService = new FavSongService(favSongRepository, new UsuarioService(usuarioRepository));

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));
        ResponseEntity<List<FavSong>> actualListAllResult = favSongController
                .listAll(new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualListAllResult.hasBody());
        assertEquals(200, actualListAllResult.getStatusCodeValue());
        assertTrue(actualListAllResult.getHeaders().isEmpty());
        verify(favSongRepository).findAllByUsuarioId(anyInt());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#listAll(Authentication)}
     */
    @Test
    void testListAll2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        ArrayList<FavSong> favSongList = new ArrayList<>();
        when(favSongRepository.findAllByUsuarioId(anyInt())).thenReturn(favSongList);
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any())).thenReturn(Optional.empty());
        FavSongService favSongService = new FavSongService(favSongRepository, new UsuarioService(usuarioRepository));

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));
        ResponseEntity<List<FavSong>> actualListAllResult = favSongController
                .listAll(new TestingAuthenticationToken("Principal", "Credentials"));
        assertEquals(favSongList, actualListAllResult.getBody());
        assertEquals(200, actualListAllResult.getStatusCodeValue());
        assertTrue(actualListAllResult.getHeaders().isEmpty());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }



    /**
     * Method under test: {@link FavSongController#listAll(Authentication)}
     */
    @Test
    void testListAll4() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.findAllByUsuarioId(anyInt())).thenReturn(new ArrayList<>());
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongService favSongService = new FavSongService(favSongRepository, usuarioService);

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));
        ResponseEntity<List<FavSong>> actualListAllResult = favSongController
                .listAll(new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualListAllResult.hasBody());
        assertEquals(200, actualListAllResult.getStatusCodeValue());
        assertTrue(actualListAllResult.getHeaders().isEmpty());
        verify(favSongRepository).findAllByUsuarioId(anyInt());
        verify(usuarioService).getByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#listAll(Authentication)}
     */
    @Test
    void testListAll5() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        when(favSongService.getAll(Mockito.<Authentication>any())).thenReturn(new ArrayList<>());
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));
        ResponseEntity<List<FavSong>> actualListAllResult = favSongController
                .listAll(new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualListAllResult.hasBody());
        assertEquals(200, actualListAllResult.getStatusCodeValue());
        assertTrue(actualListAllResult.getHeaders().isEmpty());
        verify(favSongService).getAll(Mockito.<Authentication>any());
    }

    /**
     * Method under test: {@link FavSongController#getById(int)}
     */
    @Test
    void testGetById() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.findById(anyInt())).thenReturn(Optional.of(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example")));
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(true);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        ResponseEntity<MessageDto> actualById = (new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)))).getById(1);
        assertTrue(actualById.hasBody());
        assertTrue(actualById.getHeaders().isEmpty());
        assertEquals(200, actualById.getStatusCodeValue());
        MessageDto body = actualById.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("Producto encontrado", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
        verify(favSongRepository).findById(anyInt());
    }



    /**
     * Method under test: {@link FavSongController#getById(int)}
     */
    @Test
    void testGetById3() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.findById(anyInt())).thenReturn(Optional.of(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example")));
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(false);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        ResponseEntity<MessageDto> actualById = (new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)))).getById(1);
        assertTrue(actualById.hasBody());
        assertTrue(actualById.getHeaders().isEmpty());
        assertEquals(404, actualById.getStatusCodeValue());
        MessageDto body = actualById.getBody();
        assertEquals(HttpStatus.NOT_FOUND, body.getStatus());
        assertEquals("No existe", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
    }



    /**
     * Method under test: {@link FavSongController#getById(int)}
     */
    @Test
    void testGetById5() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        ResponseEntity<MessageDto> actualById = (new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)))).getById(1);
        assertTrue(actualById.hasBody());
        assertTrue(actualById.getHeaders().isEmpty());
        assertEquals(200, actualById.getStatusCodeValue());
        MessageDto body = actualById.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("Producto encontrado", body.getMessage());
        verify(favSongService).existsById(anyInt());
        verify(favSongService).getById(anyInt());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");
        favSongDto.setSongName(null);
        favSongDto.setAlbumName(null);
        favSongDto.setArtistName(null);
        favSongDto.setSongUrl(null);
        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(400, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El campo SongName es obligatorio", body.getMessage());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");
        favSongDto.setSongName("foo");
        favSongDto.setAlbumName(null);
        favSongDto.setArtistName(null);
        favSongDto.setSongUrl(null);
        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(400, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El campo SongUrl es obligatorio", body.getMessage());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave3() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");
        favSongDto.setSongName("foo");
        favSongDto.setAlbumName(null);
        favSongDto.setArtistName(null);
        favSongDto.setSongUrl("foo");
        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(400, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El campo ArtistName es obligatorio", body.getMessage());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave4() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)));

        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");
        favSongDto.setSongName("foo");
        favSongDto.setAlbumName(null);
        favSongDto.setArtistName("foo");
        favSongDto.setSongUrl("foo");
        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(400, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El campo AlbumName es obligatorio", body.getMessage());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave5() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.save(Mockito.<FavSong>any())).thenReturn(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example"));
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(200, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSongSong Name agregada", body.getMessage());
        verify(favSongRepository).save(Mockito.<FavSong>any());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }



    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave7() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(200, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSongSong Name agregada", body.getMessage());
        verify(favSongService).save(Mockito.<FavSong>any());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave8() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any())).thenReturn(Optional.empty());
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(404, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.NOT_FOUND, body.getStatus());
        assertEquals("Usuario no encontrado", body.getMessage());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }



    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave10() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(200, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSongSong Name agregada", body.getMessage());
        verify(favSongService).save(Mockito.<FavSong>any());
        verify(usuarioService).getByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#save(FavSongDto, Authentication)}
     */
    @Test
    void testSave11() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualSaveResult = favSongController.save(favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualSaveResult.hasBody());
        assertTrue(actualSaveResult.getHeaders().isEmpty());
        assertEquals(400, actualSaveResult.getStatusCodeValue());
        MessageDto body = actualSaveResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El campo SongUrl es obligatorio", body.getMessage());
    }


    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.save(Mockito.<FavSong>any())).thenReturn(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongRepository.findById(anyInt())).thenReturn(Optional.of(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example")));
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(true);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(200, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSongSong Name actualizada", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
        verify(favSongRepository).save(Mockito.<FavSong>any());
        verify(favSongRepository).findById(anyInt());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.save(Mockito.<FavSong>any())).thenReturn(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongRepository.findById(anyInt())).thenReturn(Optional.empty());
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(true);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(404, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.NOT_FOUND, body.getStatus());
        assertEquals("FavSong no encontrada", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
        verify(favSongRepository).findById(anyInt());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate3() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.save(Mockito.<FavSong>any())).thenReturn(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongRepository.findById(anyInt())).thenReturn(Optional.of(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example")));
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(false);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(404, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.NOT_FOUND, body.getStatus());
        assertEquals("No existe", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
    }


    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate5() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(200, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSongSong Name actualizada", body.getMessage());
        verify(favSongService).existsById(anyInt());
        verify(favSongService).getById(anyInt());
        verify(favSongService).save(Mockito.<FavSong>any());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }



    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate7() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any())).thenReturn(Optional.empty());
        FavSongController favSongController = new FavSongController(favSongService,
                new UsuarioService(usuarioRepository));
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(404, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.NOT_FOUND, body.getStatus());
        assertEquals("Usuario no encontrado", body.getMessage());
        verify(favSongService).existsById(anyInt());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }


    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate9() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(200, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSongSong Name actualizada", body.getMessage());
        verify(favSongService).existsById(anyInt());
        verify(favSongService).getById(anyInt());
        verify(favSongService).save(Mockito.<FavSong>any());
        verify(usuarioService).getByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate10() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(400, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El URL de la canción es obligatorio", body.getMessage());
        verify(favSongService).existsById(anyInt());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate11() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "", "Artist Name", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(400, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El nombre de la canción es obligatorio", body.getMessage());
        verify(favSongService).existsById(anyInt());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate12() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "", "Album Name",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(400, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El nombre del artista es obligatorio", body.getMessage());
        verify(favSongService).existsById(anyInt());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate13() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto(1, "https://example.org/example", "Song Name", "Artist Name", "",
                "https://example.org/example");

        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(400, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El nombre del álbum es obligatorio", body.getMessage());
        verify(favSongService).existsById(anyInt());
    }

    /**
     * Method under test: {@link FavSongController#update(int, FavSongDto, Authentication)}
     */
    @Test
    void testUpdate14() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).save(Mockito.<FavSong>any());
        when(favSongService.getById(anyInt())).thenReturn(new FavSong(1, "https://example.org/example", "Song Name",
                "Artist Name", "Album Name", "https://example.org/example"));
        when(favSongService.existsById(anyInt())).thenReturn(true);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongController favSongController = new FavSongController(favSongService, usuarioService);
        FavSongDto favSongDto = new FavSongDto();
        ResponseEntity<MessageDto> actualUpdateResult = favSongController.update(1, favSongDto,
                new TestingAuthenticationToken("Principal", "Credentials"));
        assertTrue(actualUpdateResult.hasBody());
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(400, actualUpdateResult.getStatusCodeValue());
        MessageDto body = actualUpdateResult.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, body.getStatus());
        assertEquals("El nombre de la canción es obligatorio", body.getMessage());
        verify(favSongService).existsById(anyInt());
    }





    /**
     * Method under test: {@link FavSongController#delete(int)}
     */
    @Test
    void testDelete() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        doNothing().when(favSongRepository).deleteById(Mockito.<Integer>any());
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(true);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        ResponseEntity<MessageDto> actualDeleteResult = (new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)))).delete(1);
        assertTrue(actualDeleteResult.hasBody());
        assertTrue(actualDeleteResult.getHeaders().isEmpty());
        assertEquals(200, actualDeleteResult.getStatusCodeValue());
        MessageDto body = actualDeleteResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSong eliminada", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
        verify(favSongRepository).deleteById(Mockito.<Integer>any());
    }

    /**
     * Method under test: {@link FavSongController#delete(int)}
     */
    @Test
    void testDelete2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        doNothing().when(favSongRepository).deleteById(Mockito.<Integer>any());
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(false);
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));

        ResponseEntity<MessageDto> actualDeleteResult = (new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)))).delete(1);
        assertTrue(actualDeleteResult.hasBody());
        assertTrue(actualDeleteResult.getHeaders().isEmpty());
        assertEquals(404, actualDeleteResult.getStatusCodeValue());
        MessageDto body = actualDeleteResult.getBody();
        assertEquals(HttpStatus.NOT_FOUND, body.getStatus());
        assertEquals("No existe", body.getMessage());
        verify(favSongRepository).existsById(Mockito.<Integer>any());
    }



    /**
     * Method under test: {@link FavSongController#delete(int)}
     */
    @Test
    void testDelete4() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "Object.getClass()" because "bean" is null
        //   See https://diff.blue/R013 to resolve this issue.

        FavSongService favSongService = mock(FavSongService.class);
        doNothing().when(favSongService).delete(anyInt());
        when(favSongService.existsById(anyInt())).thenReturn(true);
        ResponseEntity<MessageDto> actualDeleteResult = (new FavSongController(favSongService,
                new UsuarioService(mock(UsuarioRepository.class)))).delete(1);
        assertTrue(actualDeleteResult.hasBody());
        assertTrue(actualDeleteResult.getHeaders().isEmpty());
        assertEquals(200, actualDeleteResult.getStatusCodeValue());
        MessageDto body = actualDeleteResult.getBody();
        assertEquals(HttpStatus.OK, body.getStatus());
        assertEquals("FavSong eliminada", body.getMessage());
        verify(favSongService).existsById(anyInt());
        verify(favSongService).delete(anyInt());
    }
}

