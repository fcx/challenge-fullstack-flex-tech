package com.challenge.Facundo.CRUD.repository;

import com.challenge.Facundo.CRUD.entity.FavSong;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class FavSongRepositoryTests {

    @Autowired
    private FavSongRepository favSongRepository;

//    @Test
//    public void testFindAllByUsuarioNombreUsuario() {
//        String username = "exampleUser";
//        List<FavSong> favSongs = favSongRepository.findAllByUsuarioNombreUsuario(username);
//        assertEquals(2, favSongs.size());
//        // Add more assertions as needed
//    }
//
//    @Test
//    public void testFindByIdAndUsuarioNombreUsuario() {
//        int id = 1;
//        String username = "exampleUser";
//        Optional<FavSong> favSongOptional = favSongRepository.findByIdAndUsuarioNombreUsuario(id, username);
//        assertTrue(favSongOptional.isPresent());
//        FavSong favSong = favSongOptional.get();
//        assertEquals(id, favSong.getId());
//        // Add more assertions as needed
//    }
}

