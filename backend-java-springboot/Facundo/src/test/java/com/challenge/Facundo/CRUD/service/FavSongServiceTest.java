package com.challenge.Facundo.CRUD.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.challenge.Facundo.CRUD.entity.FavSong;
import com.challenge.Facundo.CRUD.repository.FavSongRepository;
import com.challenge.Facundo.GLOBAL.exceptions.ResourceNotFoundException;
import com.challenge.Facundo.SECURITY.entity.Usuario;
import com.challenge.Facundo.SECURITY.repository.UsuarioRepository;
import com.challenge.Facundo.SECURITY.service.UsuarioService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

class FavSongServiceTest {
    /**
     * Method under test: {@link FavSongService#getAll(Authentication)}
     */
    @Test
    void testGetAll() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        ArrayList<FavSong> favSongList = new ArrayList<>();
        when(favSongRepository.findAllByUsuarioId(anyInt())).thenReturn(favSongList);
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongService favSongService = new FavSongService(favSongRepository, new UsuarioService(usuarioRepository));
        List<FavSong> actualAll = favSongService.getAll(new TestingAuthenticationToken("Principal", "Credentials"));
        assertSame(favSongList, actualAll);
        assertTrue(actualAll.isEmpty());
        verify(favSongRepository).findAllByUsuarioId(anyInt());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }

    /**
     * Method under test: {@link FavSongService#getAll(Authentication)}
     */
    @Test
    void testGetAll2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.findAllByUsuarioId(anyInt())).thenReturn(new ArrayList<>());
        UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
        when(usuarioRepository.findByNombreUsuario(Mockito.<String>any())).thenReturn(Optional.empty());
        FavSongService favSongService = new FavSongService(favSongRepository, new UsuarioService(usuarioRepository));
        assertTrue(favSongService.getAll(new TestingAuthenticationToken("Principal", "Credentials")).isEmpty());
        verify(usuarioRepository).findByNombreUsuario(Mockito.<String>any());
    }



    /**
     * Method under test: {@link FavSongService#getAll(Authentication)}
     */
    @Test
    void testGetAll4() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        ArrayList<FavSong> favSongList = new ArrayList<>();
        when(favSongRepository.findAllByUsuarioId(anyInt())).thenReturn(favSongList);
        UsuarioService usuarioService = mock(UsuarioService.class);
        when(usuarioService.getByNombreUsuario(Mockito.<String>any()))
                .thenReturn(Optional.of(new Usuario("Nombre", "Nombre Usuario", "jane.doe@example.org", "iloveyou")));
        FavSongService favSongService = new FavSongService(favSongRepository, usuarioService);
        List<FavSong> actualAll = favSongService.getAll(new TestingAuthenticationToken("Principal", "Credentials"));
        assertSame(favSongList, actualAll);
        assertTrue(actualAll.isEmpty());
        verify(favSongRepository).findAllByUsuarioId(anyInt());
        verify(usuarioService).getByNombreUsuario(Mockito.<String>any());
    }



    /**
     * Method under test: {@link FavSongService#getById(int)}
     */
    @Test
    void testGetById() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        FavSong favSong = new FavSong(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        when(favSongRepository.findById(anyInt())).thenReturn(Optional.of(favSong));
        assertSame(favSong,
                (new FavSongService(favSongRepository, new UsuarioService(mock(UsuarioRepository.class)))).getById(1));
        verify(favSongRepository).findById(anyInt());
    }

    /**
     * Method under test: {@link FavSongService#getById(int)}
     */
    @Test
    void testGetById2() throws ResourceNotFoundException {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class,
                () -> (new FavSongService(favSongRepository, new UsuarioService(mock(UsuarioRepository.class)))).getById(1));
        verify(favSongRepository).findById(anyInt());
    }

    /**
     * Method under test: {@link FavSongService#save(FavSong)}
     */
    @Test
    void testSave() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.save(Mockito.<FavSong>any())).thenReturn(new FavSong(1, "https://example.org/example",
                "Song Name", "Artist Name", "Album Name", "https://example.org/example"));
        FavSongService favSongService = new FavSongService(favSongRepository,
                new UsuarioService(mock(UsuarioRepository.class)));
        favSongService.save(new FavSong(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example"));
        verify(favSongRepository).save(Mockito.<FavSong>any());
    }

    /**
     * Method under test: {@link FavSongService#delete(int)}
     */
    @Test
    void testDelete() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        doNothing().when(favSongRepository).deleteById(Mockito.<Integer>any());
        (new FavSongService(favSongRepository, new UsuarioService(mock(UsuarioRepository.class)))).delete(1);
        verify(favSongRepository).deleteById(Mockito.<Integer>any());
    }

    /**
     * Method under test: {@link FavSongService#existsById(int)}
     */
    @Test
    void testExistsById() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(true);
        assertTrue(
                (new FavSongService(favSongRepository, new UsuarioService(mock(UsuarioRepository.class)))).existsById(1));
        verify(favSongRepository).existsById(Mockito.<Integer>any());
    }

    /**
     * Method under test: {@link FavSongService#existsById(int)}
     */
    @Test
    void testExistsById2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R026 Failed to create Spring context.
        //   Attempt to initialize test context failed with
        //   com.diffblue.fuzztest.shared.proxy.BeanInstantiationException: Could not instantiate bean: class org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.IllegalStateException: Could not load CacheAwareContextLoaderDelegate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]
        //       at java.util.Optional.map(Optional.java:260)
        //   org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate]: Constructor threw exception; nested exception is java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.NoClassDefFoundError: org/springframework/core/io/support/SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   java.lang.ClassNotFoundException: org.springframework.core.io.support.SpringFactoriesLoader$FailureHandler
        //       at java.util.Optional.map(Optional.java:260)
        //   See https://diff.blue/R026 to resolve this issue.

        FavSongRepository favSongRepository = mock(FavSongRepository.class);
        when(favSongRepository.existsById(Mockito.<Integer>any())).thenReturn(false);
        assertFalse(
                (new FavSongService(favSongRepository, new UsuarioService(mock(UsuarioRepository.class)))).existsById(1));
        verify(favSongRepository).existsById(Mockito.<Integer>any());
    }
}

