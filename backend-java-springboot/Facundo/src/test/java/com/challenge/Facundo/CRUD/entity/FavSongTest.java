package com.challenge.Facundo.CRUD.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class FavSongTest {
    /**
     * Method under test: {@link FavSong#FavSong(int, String, String, String, String, String)}
     */
    @Test
    void testConstructor() {
        FavSong actualFavSong = new FavSong(1, "https://example.org/example", "Song Name", "Artist Name", "Album Name",
                "https://example.org/example");

        assertEquals("https://example.org/example", actualFavSong.getAlbumCoverUrl());
        assertEquals(0, actualFavSong.getUsuarioId());
        assertEquals("https://example.org/example", actualFavSong.getSongUrl());
        assertEquals("Song Name", actualFavSong.getSongName());
        assertEquals(1, actualFavSong.getId());
        assertEquals("Artist Name", actualFavSong.getArtistName());
        assertEquals("Album Name", actualFavSong.getAlbumName());
    }
}

