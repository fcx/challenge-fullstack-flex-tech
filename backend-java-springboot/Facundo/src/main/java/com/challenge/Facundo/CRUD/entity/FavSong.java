package com.challenge.Facundo.CRUD.entity;

//import com.challenge.Facundo.SECURITY.entity.User;
import com.challenge.Facundo.SECURITY.entity.Usuario;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class FavSong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "usuario_id")
    private int usuarioId;


    private String songUrl;
    private String songName;
    private String artistName;
    private String albumName;
    private String albumCoverUrl;

    public FavSong(int id, String songUrl, String songName, String artistName, String albumName, String albumCoverUrl) {
        this.id = id;
        this.songUrl = songUrl;
        this.songName = songName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.albumCoverUrl = albumCoverUrl;
    }
}
