package com.challenge.Facundo.GLOBAL.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Mensaje {
    private String mensaje;
}
