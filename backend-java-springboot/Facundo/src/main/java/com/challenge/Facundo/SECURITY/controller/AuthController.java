package com.challenge.Facundo.SECURITY.controller;

import com.challenge.Facundo.GLOBAL.dto.Mensaje;
import com.challenge.Facundo.GLOBAL.enums.RolNombre;
import com.challenge.Facundo.GLOBAL.exceptions.ResourceNotFoundException;
import com.challenge.Facundo.SECURITY.dto.JwtDto;
import com.challenge.Facundo.SECURITY.dto.LoginUsuario;
import com.challenge.Facundo.SECURITY.dto.NuevoUsuario;
import com.challenge.Facundo.SECURITY.entity.Rol;
import com.challenge.Facundo.SECURITY.entity.Usuario;
import com.challenge.Facundo.SECURITY.jwt.JwtProvider;
import com.challenge.Facundo.SECURITY.service.RolService;
import com.challenge.Facundo.SECURITY.service.UsuarioService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    private final UsuarioService usuarioService;

    private final RolService rolService;

    private final JwtProvider jwtProvider;

    @Autowired
    public AuthController(PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, UsuarioService usuarioService, RolService rolService, JwtProvider jwtProvider) {
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.usuarioService = usuarioService;
        this.rolService = rolService;
        this.jwtProvider = jwtProvider;
    }

    @Operation(summary = "create a user , can be a ROLE_ADMIN or a ROLE_USER")
    @PostMapping("/create")
    public ResponseEntity<?> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return new ResponseEntity<>(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
        if (usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
            return new ResponseEntity<>(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if (usuarioService.existsByEmail(nuevoUsuario.getEmail()))
            return new ResponseEntity<>(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);

        Usuario usuario = new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(),
                nuevoUsuario.getEmail(), passwordEncoder.encode(nuevoUsuario.getPassword()));

        Set<Rol> roles = new HashSet<>();
        Rol defaultRol = new Rol(RolNombre.ROLE_USER);
        rolService.getByRolNombre(RolNombre.ROLE_USER).ifPresentOrElse(roles::add, () -> roles.add(defaultRol));

        if (nuevoUsuario.getRoles().contains("admin")) {
            Optional<Rol> adminRoleOptional = rolService.getByRolNombre(RolNombre.ROLE_ADMIN);
            if (adminRoleOptional.isPresent()) {
                roles.add(adminRoleOptional.get());
            } else {
                Rol adminRole = new Rol(RolNombre.ROLE_ADMIN);
                roles.add(adminRole);
            }
        }

        usuario.setRoles(roles);
        usuarioService.save(usuario);

        return new ResponseEntity<>(new Mensaje("usuario guardado"), HttpStatus.CREATED);
    }
    @Operation(summary = "create a user , only can be a ROLE_USER , sera utilizado en el create-user del frontend")
    @PostMapping("/create-user")
    public ResponseEntity<?> createUser(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult) throws ResourceNotFoundException {
        if (bindingResult.hasErrors())
            return new ResponseEntity<>(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
        if (usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
            return new ResponseEntity<>(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if (usuarioService.existsByEmail(nuevoUsuario.getEmail()))
            return new ResponseEntity<>(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);

        Usuario usuario = new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(),
                nuevoUsuario.getEmail(), passwordEncoder.encode(nuevoUsuario.getPassword()));

        Rol defaultUserRole = rolService.getByRolNombre(RolNombre.ROLE_USER)
                .orElseThrow(() -> new ResourceNotFoundException("Rol not found"));

        Set<Rol> roles = new HashSet<>();
        roles.add(defaultUserRole);

        usuario.setRoles(roles);
        usuarioService.save(usuario);

        return new ResponseEntity<>(new Mensaje("usuario " +  usuario.getNombreUsuario() + " guardado" ), HttpStatus.CREATED);
    }



    @Operation(summary = "Login with registered username and password")
    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos"), HttpStatus.BAD_REQUEST);
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        JwtDto jwtDto = new JwtDto(jwt);
        return new ResponseEntity<>(jwtDto, HttpStatus.OK);
    }

    @PostMapping("/refresh")
    public ResponseEntity<JwtDto> refresh(@RequestBody JwtDto jwtDto) throws ParseException {
        String token = jwtProvider.refreshToken(jwtDto);
        JwtDto jwt = new JwtDto(token);
        return new ResponseEntity<>(jwt, HttpStatus.OK);
    }
}
