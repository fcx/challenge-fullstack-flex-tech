package com.challenge.Facundo.SECURITY.repository;

import com.challenge.Facundo.GLOBAL.enums.RolNombre;
import com.challenge.Facundo.SECURITY.entity.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {
    Optional<Rol> findByRolNombre(RolNombre rolNombre);

}
