package com.challenge.Facundo.CRUD.repository;

import com.challenge.Facundo.CRUD.entity.FavSong;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FavSongRepository extends JpaRepository<FavSong, Integer> {
    //    Optional<FavSong> findByUser(String user);
    Optional<FavSong> findById(int id);
//    boolean existsBySongUrl(String songUrl);
List<FavSong> findAllByUsuarioId(int usuarioId);
}
