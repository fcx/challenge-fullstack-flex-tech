package com.challenge.Facundo.CRUD.service;

import com.challenge.Facundo.CRUD.entity.FavSong;
import com.challenge.Facundo.CRUD.repository.FavSongRepository;
import com.challenge.Facundo.GLOBAL.exceptions.ResourceNotFoundException;
import com.challenge.Facundo.SECURITY.entity.Usuario;
import com.challenge.Facundo.SECURITY.service.UsuarioService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Collections;

@Service
@Transactional
public class FavSongService {

    private final FavSongRepository favSongRepository;
    private final UsuarioService usuarioService;

    @Autowired
    public FavSongService( FavSongRepository favSongRepository , UsuarioService usuarioService) {
        this.favSongRepository = favSongRepository;
        this.usuarioService = usuarioService;
    }


    public List<FavSong> getAll(Authentication authentication) {
        String username = authentication.getName();
        Optional<Usuario> optionalUsuario = usuarioService.getByNombreUsuario(username);
        if (optionalUsuario.isPresent()) {
            Usuario usuario = optionalUsuario.get();
            return favSongRepository.findAllByUsuarioId(usuario.getId());
        }
        return new ArrayList<>();
    }

    public FavSong getById(int id) throws ResourceNotFoundException {
        return favSongRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));
    }

    public void save(FavSong favSong) {
        favSongRepository.save(favSong);
    }
    public void delete(int id) {
        favSongRepository.deleteById(id);
    }
    public  boolean existsById(int id) {
        return favSongRepository.existsById(id);
    }


}
