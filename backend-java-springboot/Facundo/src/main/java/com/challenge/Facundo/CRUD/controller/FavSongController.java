package com.challenge.Facundo.CRUD.controller;

import com.challenge.Facundo.CRUD.dto.FavSongDto;
import com.challenge.Facundo.CRUD.entity.FavSong;
import com.challenge.Facundo.CRUD.service.FavSongService;
import com.challenge.Facundo.GLOBAL.dto.MessageDto;
import com.challenge.Facundo.SECURITY.entity.Usuario;
import com.challenge.Facundo.SECURITY.service.UsuarioService;
import com.challenge.Facundo.GLOBAL.exceptions.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import io.micrometer.common.util.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/favSong")
public class FavSongController {

    private final FavSongService favSongService;

    private final UsuarioService usuarioService;

    @Autowired
    public FavSongController(FavSongService favSongService, UsuarioService usuarioService) {
        this.favSongService = favSongService;
        this.usuarioService = usuarioService;
    }


    @Operation( summary = "Devuelve todos los favoritos de un usuario")
    @GetMapping
    public ResponseEntity<List<FavSong>> listAll(Authentication authentication) {
        List<FavSong> favSongs = favSongService.getAll(authentication);
        return ResponseEntity.ok(favSongs);
    }

    @Operation( summary = "Devuelve una cancion favorita por su ID")
    @GetMapping("/{id}")
    public ResponseEntity<MessageDto> getById(@PathVariable("id") int id) throws ResourceNotFoundException {
        if(!favSongService.existsById(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new MessageDto(HttpStatus.NOT_FOUND, "No existe"));
        }
        FavSong favSong = favSongService.getById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new MessageDto(HttpStatus.OK, "Producto encontrado"));
    }

    @Operation( summary = "Guarda una cancion favorita , hace la logica de guardar en la base de datos")
    @PostMapping
    public ResponseEntity<MessageDto> save(@RequestBody FavSongDto favSongDto, Authentication authentication) {
        if (StringUtils.isBlank(favSongDto.getSongName()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El campo SongName es obligatorio"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(favSongDto.getSongUrl()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El campo SongUrl es obligatorio"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(favSongDto.getArtistName()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El campo ArtistName es obligatorio"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(favSongDto.getAlbumName()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El campo AlbumName es obligatorio"), HttpStatus.BAD_REQUEST);

        String username = authentication.getName();

        Optional<Usuario> optionalUsuario = usuarioService.getByNombreUsuario(username);
        if (optionalUsuario.isEmpty()) {
            return new ResponseEntity<>(new MessageDto(HttpStatus.NOT_FOUND, "Usuario no encontrado"), HttpStatus.NOT_FOUND);
        }

        Usuario usuario = optionalUsuario.get();

        FavSong favSong = new FavSong();
        favSong.setId(favSongDto.getId());
        favSong.setUsuarioId(usuario.getId());
        favSong.setSongName(favSongDto.getSongName());
        favSong.setSongUrl(favSongDto.getSongUrl());
        favSong.setArtistName(favSongDto.getArtistName());
        favSong.setAlbumName(favSongDto.getAlbumName());
        favSong.setAlbumCoverUrl(favSongDto.getAlbumCoverUrl());

        favSongService.save(favSong);

        return new ResponseEntity<>(new MessageDto(HttpStatus.OK, "FavSong"+ favSong.getSongName() + " agregada"), HttpStatus.OK);
    }
    @Operation( summary = "Actualiza una cancion favorita , hace la logica de guardar en la base de datos")
    @PutMapping("/{id}")
    public ResponseEntity<MessageDto> update(@PathVariable("id") int id, @RequestBody FavSongDto favSongDto, Authentication authentication) {
        if (!favSongService.existsById(id))
            return new ResponseEntity<>(new MessageDto(HttpStatus.NOT_FOUND, "No existe"), HttpStatus.NOT_FOUND);
        if (StringUtils.isBlank(favSongDto.getSongName()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El nombre de la canción es obligatorio"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(favSongDto.getSongUrl()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El URL de la canción es obligatorio"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(favSongDto.getArtistName()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El nombre del artista es obligatorio"), HttpStatus.BAD_REQUEST);
        if (StringUtils.isBlank(favSongDto.getAlbumName()))
            return new ResponseEntity<>(new MessageDto(HttpStatus.BAD_REQUEST, "El nombre del álbum es obligatorio"), HttpStatus.BAD_REQUEST);

        String username = authentication.getName();

        Optional<Usuario> optionalUsuario = usuarioService.getByNombreUsuario(username);
        if (!optionalUsuario.isPresent()) {
            return new ResponseEntity<>(new MessageDto(HttpStatus.NOT_FOUND, "Usuario no encontrado"), HttpStatus.NOT_FOUND);
        }

        Usuario usuario = optionalUsuario.get();

        try {
            FavSong favSong = favSongService.getById(id);
            favSong.setUsuarioId(usuario.getId());
            favSong.setSongName(favSongDto.getSongName());
            favSong.setSongUrl(favSongDto.getSongUrl());
            favSong.setArtistName(favSongDto.getArtistName());
            favSong.setAlbumName(favSongDto.getAlbumName());
            favSong.setAlbumCoverUrl(favSongDto.getAlbumCoverUrl());

            favSongService.save(favSong);

            return new ResponseEntity<>(new MessageDto(HttpStatus.OK, "FavSong"+ favSong.getSongName() + " actualizada"), HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            return new ResponseEntity<>(new MessageDto(HttpStatus.NOT_FOUND, "FavSong no encontrada"), HttpStatus.NOT_FOUND);
        }
    }




    @Operation( summary = "Elimina una cancion favorita , hace la logica de eliminar de la base de datos")
    @DeleteMapping("/{id}")
    public ResponseEntity<MessageDto> delete(@PathVariable("id") int id) {
        if (!favSongService.existsById(id))
            return new ResponseEntity<>(new MessageDto(HttpStatus.NOT_FOUND, "No existe"), HttpStatus.NOT_FOUND);

        favSongService.delete(id);
        return new ResponseEntity<>(new MessageDto(HttpStatus.OK, "FavSong eliminada"), HttpStatus.OK);
    }

}
