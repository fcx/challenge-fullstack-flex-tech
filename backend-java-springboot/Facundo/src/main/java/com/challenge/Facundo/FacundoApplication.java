package com.challenge.Facundo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacundoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacundoApplication.class, args);
	}

}
