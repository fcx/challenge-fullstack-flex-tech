package com.challenge.Facundo.CRUD.dto;

//import com.challenge.Facundo.SECURITY.entity.User;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class FavSongDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
//    @NotNull
//    private User user;
    @NotNull
    private String songUrl;
    @NotNull
    private String songName;
    @NotNull
    private String artistName;
    @NotNull
    private String AlbumName;
    private String albumCoverUrl;
}
