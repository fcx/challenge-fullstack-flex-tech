import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from 'src/enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  private headers: HttpHeaders;
  private clientId = environment.clientId;
  private clientSecret = environment.clientSecret;
  private accessToken = '';

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
  }

  private getAccessToken(): Observable<any> {
    const tokenUrl = 'https://accounts.spotify.com/api/token';
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(`${this.clientId}:${this.clientSecret}`))
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const body = 'grant_type=client_credentials';

    return this.httpClient.post(tokenUrl, body, { headers });
  }

  private setAccessToken(token: string): void {
    this.accessToken = token;
    this.headers = this.headers.set('Authorization', `Bearer ${this.accessToken}`);
  }

  searchTrack(q: string): Observable<any> {
    const url = `${environment.apiBaseUrl}/search?q=${q}&type=track&limit=10`;
    return this.httpClient.get(url, { headers: this.headers });
  }
  private getQuery(query: string): Observable<any> {
    const url = `https://api.spotify.com/v1/${query}`;
    return this.httpClient.get(url, { headers: this.headers });
  }

  getNewReleases(): Observable<any> {
    return this.getQuery('browse/new-releases')
      .pipe(map((data: any) => data.albums.items));
  }

  getFeaturedPlaylists(): Observable<any> {
    return this.getQuery('browse/featured-playlists')
      .pipe(map((data: any) => data.playlists.items));
  }

  getArtistas(termino: string): Observable<any> {
    return this.getQuery(`search?q=${termino}&type=artist&limit=5`)
      .pipe(map((data: any) => data.artists.items));
  }

  getPlaylist(id: string): Observable<any> {
    return this.getQuery(`playlists/${id}`);
  }

  getArtista(id: string): Observable<any> {
    return this.getQuery(`artists/${id}`);
  }
  getSearch(termino: string, type: string): Observable<any[]> {
    const query = `search?q=${termino}&type=${type}&limit=15`;
    return this.getQuery(query)
      .pipe(map((data: any) => data[type + 's'].items));
  }

  getTopTracks(id: string): Observable<any> {
    return this.getQuery(`artists/${id}/top-tracks?market=US`)
      .pipe(map((data: any) => data.tracks));
  }
  getAlbum(id: string): Observable<any> {
    return this.getQuery(`albums/${id}`);
  }
  getAlbumTracks(albumId: string): Observable<any> {
    return this.getQuery(`albums/${albumId}/tracks`);
  }

  getAlbums(id: string): Observable<any[]> {
    return this.getQuery(`artists/${id}/albums`)
      .pipe(map((data: any) => data.items));
  }
  getTopPlaylistTracks(id: string): Observable<any> {
    return this.getQuery(`playlists/${id}/tracks&limit=5`)
      .pipe(map((data: any) => data.tracks));
  }

  initialize(): Observable<any> {
    return new Observable((observer) => {
      this.getAccessToken().subscribe(
        (response: any) => {
          const accessToken = response.access_token;
          this.setAccessToken(accessToken);
          observer.next();
          observer.complete();
        },
        (error) => {
          observer.error(error);
          observer.complete();
        }
      );
    });
  }

}
