import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/enviroments/enviroment';
import { LoginUserDto } from '../models/login-user-dto';
import { Observable } from 'rxjs';
import { JwtTokenDto } from '../models/jwt-token-dto';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authURL = environment.apiRestUrl + '/auth/';
  private _isLogged = false;

  constructor(private httpClient: HttpClient) { }

  public login(dto: LoginUserDto): Observable<JwtTokenDto> {
    return this.httpClient.post<JwtTokenDto>(this.authURL + 'login', dto);
  }

  get getIsLogged(): boolean {
    return this._isLogged;
  }
  set setIsLogged(bol: boolean) {
    this._isLogged = bol;
  }
}
