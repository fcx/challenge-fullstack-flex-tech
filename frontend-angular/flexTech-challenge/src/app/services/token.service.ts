import { Injectable } from '@angular/core';

const tokenKey = "AuthToken";

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  public setToken(token: string): void {
    localStorage.setItem(tokenKey, token);
  }

  public getToken(): string | null {
    return localStorage.getItem(tokenKey);
  }
  public logOut(): void {
    localStorage.removeItem(tokenKey);
  }

  public isLogged(): boolean {
    return this.getToken() != null;
  }

  public isAdmin(): boolean {
    if (!this.isLogged()) {
      return false;
    }
    const token = this.getToken();
    const payload = token?.split(".")[1];
    const payloadDecoded = atob(payload!);
    const values = JSON.parse(payloadDecoded);
    const roles = values.roles;
    if (roles.indexOf('ROLE_ADMIN') < 0) {
      return false;
    }
    return true;
  }
  public getUsername(): string {
    if (!this.isLogged) {
      return '';
    }
    const token = this.getToken();
    const payload = token?.split(".")[1];
    const payloadDecoded = atob(payload!);
    const values = JSON.parse(payloadDecoded);
    const roles = values.roles;
    return values.sub;
  }
}
