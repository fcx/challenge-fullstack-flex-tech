import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/enviroments/enviroment';
import { FavSongDto } from '../models/fav-song-dto';

@Injectable({
  providedIn: 'root'
})
export class FavSongService {

  productURL = environment.apiRestUrl + '/favSong';

  constructor(private httpCliente: HttpClient) { }

  public list(): Observable<FavSongDto[]> {
    return this.httpCliente.get<FavSongDto[]>(this.productURL);
  }
  public detail(id: number): Observable<FavSongDto> {
    return this.httpCliente.get<FavSongDto>(this.productURL + '/' + id);
  }
  public create(dto: FavSongDto): Observable<FavSongDto> {
    return this.httpCliente.post<FavSongDto>(this.productURL, dto);
  }
  public update(id: number, dto: FavSongDto): Observable<FavSongDto> {
    return this.httpCliente.put<FavSongDto>(this.productURL + `/${id}`, dto);
  }
  public delete(id: number): Observable<FavSongDto> {
    return this.httpCliente.delete<any>(this.productURL + `/${id}`);
  }
}
