import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginUserDto } from 'src/app/models/login-user-dto';
import { AuthService } from 'src/app/services/auth.service';
import { SpotifyService } from 'src/app/services/spotify.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {


  logeado = this.authService.getIsLogged;

  form = new FormGroup({
    nombreUsuario: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  })

  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private toast: ToastrService,
    private router: Router,
    private spotifyService: SpotifyService
  ) { }

  ngOnInit(): void {

  }


  onLogin() {
    const dto = new LoginUserDto(this.form.value.nombreUsuario!, this.form.value.password!);
    this.authService.login(dto).subscribe(
      data => {
        this.tokenService.setToken(data.token);
        window.location.reload();
        this.router.navigate(['/menu']);
        this.toast.success('Inicio de sesión exitoso', 'Éxito', { timeOut: 3000, positionClass: 'toast-top-center' });
      },
      err => {
        this.toast.error(err.error.message, 'Error', { timeOut: 3000, positionClass: 'toast-top-center' });
        this.router.navigate(['/login']);
      }
    )
  }


}
