import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ArtistComponent } from './components/pages/artist/artist.component';

import { ProfileComponent } from './components/pages/profile/profile.component';
import { SearchComponent } from './components/pages/search/search.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AlbumComponent } from './components/pages/album/album.component';
import { AboutMeComponent } from './components/pages/about-me/about-me.component';
import { HomeGuard } from './guards/home.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },

  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'register', component: RegisterComponent },

  { path: 'home', component: HomePageComponent, canActivate: [HomeGuard], data: { expectedRoles: ['admin', 'user'] } },
  { path: 'artist/:id', component: ArtistComponent, canActivate: [HomeGuard], data: { expectedRoles: ['admin', 'user'] } },
  { path: 'album/:id', component: AlbumComponent, canActivate: [HomeGuard], data: { expectedRoles: ['admin', 'user'] } },
  { path: 'profile', component: ProfileComponent, canActivate: [HomeGuard], data: { expectedRoles: ['admin', 'user'] } },
  { path: 'search', component: SearchComponent, canActivate: [HomeGuard], data: { expectedRoles: ['admin', 'user'] } },
  { path: 'aboutMe', component: AboutMeComponent },

  { path: '**', redirectTo: 'login' },
  { path: '', redirectTo: 'login', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
