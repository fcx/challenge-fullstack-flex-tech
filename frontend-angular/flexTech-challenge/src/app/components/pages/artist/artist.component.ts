import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';
import { faAdd, faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  artista: any = {};
  topTracks: any[] = [];
  albums: any = [];
  faCheck = faCheck;
  faAdd = faAdd;

  constructor(private spotifyService: SpotifyService, private activatedRoute: ActivatedRoute, private _router: Router) {

    this.activatedRoute.params.subscribe(params => {
      this.getArtista(params['id']);
      this.getTopTracks(params['id']);
      this.getAlbums(params['id']);
    })
  }

  ngOnInit(): void {
    // this activate when logged via backend with jwt
    this.spotifyService.initialize();
  }
  getArtista(id: string) {
    this.spotifyService.getArtista(id).subscribe(
      artista => {
        this.artista = artista;
      }
    );
  }
  getTopTracks(id: string) {
    this.spotifyService.getTopTracks(id).subscribe((topTracks) => {
      this.topTracks = topTracks.map((track: any) => {
        // Assign the album cover URL to each track
        track.albumCoverUrl = track.album.images[0].url;
        return track;
      });
    });
  }

  viewAlbum(album: any) {
    const albumID = album.id;
    this._router.navigate(['/album', albumID]);
  }

  getAlbums(id: string) {
    this.spotifyService.getAlbums(id).subscribe(
      albums => {
        this.albums = albums;
      }
    );
  }

}
