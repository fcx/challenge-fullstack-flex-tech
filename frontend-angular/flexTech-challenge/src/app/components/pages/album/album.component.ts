import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faAdd, faCheck } from '@fortawesome/free-solid-svg-icons';
import { FavSongDto } from 'src/app/models/fav-song-dto';
import { FavSongService } from 'src/app/services/fav-song.service';
import { SpotifyService } from 'src/app/services/spotify.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent {

  faCheck = faCheck;
  faAdd = faAdd;
  album: any = {};
  albumTracks: any[] = [];

  constructor(
    private spotifyService: SpotifyService,
    private activatedRoute: ActivatedRoute,
    private _router: Router,
    private favSongService: FavSongService,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      const albumId = params['id'];
      this.getAlbum(albumId);
      this.getAlbumTracks(albumId);
    });
  }

  getAlbum(albumId: string) {
    this.spotifyService.getAlbum(albumId).subscribe(
      album => {
        this.album = album;
      }
    );
  }

  getAlbumTracks(albumId: string) {
    this.spotifyService.getAlbumTracks(albumId).subscribe(
      albumTracks => {
        this.albumTracks = albumTracks.items; // Assuming the tracks are returned as an array of items
      }
    );
  }

  viewTrack(track: any) {
    const trackId = track.id;
    this._router.navigate(['/track', trackId]);
  }
  addFavSong(track: any): void {
    const favSongDto = new FavSongDto(track.id, track.songUrl, track.name, track.artists[0].name, track.albumCoverUrl, track.album.name);
    this.favSongService.create(favSongDto).subscribe(
      data => {
        this.toast.success("Success", 'Success', { timeOut: 3000, positionClass: 'toast-top-center' });
        console.log(favSongDto);
      },
      err => {
        this.toast.error("Error", 'Error', { timeOut: 3000, positionClass: 'toast-top-center' });
      }
    )
  }

}
