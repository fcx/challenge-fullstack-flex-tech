import { Component, OnInit } from '@angular/core';
import { faAdd, faCheck } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { FavSongDto } from 'src/app/models/fav-song-dto';
import { FavSongService } from 'src/app/services/fav-song.service';
import { SpotifyService } from 'src/app/services/spotify.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  artista: any = {};
  topTracks: any[] = [];
  albums: any = [];
  faCheck = faCheck;
  faAdd = faAdd;
  isAdmin: boolean = false;
  isLogged: boolean = false;
  user!: string;


  favSongs: any[] = [];

  constructor(
    private favSongService: FavSongService,
    private toast: ToastrService,
    private tokenService: TokenService,
    private spotifyService: SpotifyService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.tokenService.isAdmin();
    this.isLogged = this.tokenService.isLogged();
    this.user = this.tokenService.getUsername();
    this.getFavSongs();
  }
  getFavSongs(): void {
    this.favSongService.list().subscribe(
      data => {
        console.log(data);
        this.favSongs = data;
      },
      err => {
        this.toast.error("Error", 'Error', { timeOut: 3000, positionClass: 'toast-top-center' });
      }
    )
  }


}
