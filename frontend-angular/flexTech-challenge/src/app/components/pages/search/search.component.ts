import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { finalize, map } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { SpotifyService } from 'src/app/services/spotify.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchType: string = 'artist';
  artistasSearch: any[] = [];
  isAdmin: boolean = false;
  isLogged: boolean = false;
  user!: string;

  constructor(private spotifyService: SpotifyService, private _router: Router, private tokenService: TokenService) { }

  ngOnInit(): void {
    this.isAdmin = this.tokenService.isAdmin();
    this.isLogged = this.tokenService.isLogged();
    this.user = this.tokenService.getUsername();
    this.spotifyService.initialize();
  }

  search(termino: string) {
    if (termino.trim() === '') {
      this.artistasSearch = [];
      return;
    }

    this.spotifyService.getSearch(termino, this.searchType)
      .subscribe((items: any[]) => {
        this.artistasSearch = items;
      });
  }
  changeSearchType(type: string) {
    this.searchType = type;
    // Limpiar los resultados de búsqueda
    this.artistasSearch = [];
    console.log(this.searchType)
  }


  viewArtist(item: any) {
    let artistID;
    if (item.type === 'artist') {
      artistID = item.id;
    } else {
      artistID = item.artists[0].id;
    }
    this._router.navigate(['/artist', artistID]);
  }

}
