import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RouteService } from 'src/app/services/route.service';
import { TokenService } from 'src/app/services/token.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  dropdownOpen: boolean = false;
  user!: string;
  logeado!: boolean;

  constructor(private router: Router, private route: ActivatedRoute, private routeService: RouteService, private tokenService: TokenService, private toast: ToastrService) { }

  ngOnInit(): void {
    this.user = this.tokenService.getUsername();
    this.logeado = this.tokenService.isLogged();
  }



  isActiveRoute(routePath: string): boolean {
    return this.router.url.includes(routePath);
  }
  isCurrentRoute(route: string): boolean {
    return this.routeService.currentRoute.includes(route);
  }
  goBack(): void {
    this.router.navigateByUrl(this.getPreviousUrl());
  }

  private getPreviousUrl(): string {
    const previousUrl = sessionStorage.getItem('previousUrl');
    sessionStorage.removeItem('previousUrl');
    return previousUrl || '/';
  }

  getTime(): string {
    const currentTime = new Date();
    const currentHour = currentTime.getHours();

    if (currentHour >= 5 && currentHour < 12) {
      return "morning";
    } else if (currentHour >= 12 && currentHour < 18) {
      return "afternoon";
    } else {
      return "night";
    }
  }
  closeSession(): void {
    Swal.fire({
      title: 'Cerrar sesión',
      text: '¿Estás seguro de que quieres cerrar sesión?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.tokenService.logOut();
        this.router.navigate(['/login']);
        this.toast.success(this.user, '¡Has cerrado sesión!', { timeOut: 5000, positionClass: 'toast-top-center' });
      }
    });
  }
}
