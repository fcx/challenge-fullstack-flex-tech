import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, of, switchMap } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { SpotifyService } from 'src/app/services/spotify.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent {

  @Input() items: any[] = [];

  artista: any = {};
  topTracks: any = {};
  loadingArtist?: boolean;
  artistas: any[] = [];
  loadingArtistas?: boolean;

  isAdmin: boolean = false;
  isLogged: boolean = false;
  user!: string;


  constructor(private spotifyService: SpotifyService, private _router: Router, private router: ActivatedRoute, private authService: AuthService, private tokenService: TokenService) { }

  ngOnInit(): void {
    this.isAdmin = this.tokenService.isAdmin();
    this.isLogged = this.tokenService.isLogged();
    this.user = this.tokenService.getUsername();
    this.spotifyService.initialize().subscribe(
      data => {
        this.getArtistas();
      }
    )
  }


  getArtista(id: string) {
    this.spotifyService.getArtista(id)
      .subscribe(artista => {
        console.log(artista)
        this.artista = artista
        this.loadingArtist = false;
      })
  }
  getArtistas() {
    const artistIds = ['2YZyLoL8N0Wb9xBt1NhZWg', '2pAWfrd7WFF3XhVt9GooDL', '09hVIj6vWgoCDtT03h8ZCa', '73sIBHcqh3Z3NyqHKZ7FOL', '4frXpPxQQZwbCu3eTGnZEw', '6CrQKZeuSKNYgrE7PeYqJ1', '3jO7X5KupvwmWTHGtHgcgo', '1bAftSH8umNcGZ0uyV7LMg']; // Replace with your desired artist IDs
    const artistRequests = artistIds.map(id => this.spotifyService.getArtista(id));

    forkJoin(artistRequests).subscribe(
      artistResponses => {
        this.artistas = artistResponses;
        this.loadingArtistas = false;
      },
      error => {
        console.error('Failed to get artists:', error);
      }
    );
  }

  getTopTracks(id: string) {
    this.spotifyService.getTopTracks(id)
      .subscribe(tracks => {
        console.log(tracks)
        this.topTracks = tracks
      })
  }
  viewArtist(item: any) {
    let artistID;
    if (item.type === 'artist') {
      artistID = item.id;
    } else {
      artistID = item.artists[0].id;
    }
    this._router.navigate(['/artist', artistID]);
  }
}



