import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  @Input() imageUrl!: string;
  @Input() title!: string;
  @Input() cardSize: string = 'w-80';
  @Input() type: string = 'track';
  @Input() artist: string = '';
  @Input() album: string = '';


  onImageError(): void {
    this.imageUrl = '../../../assets/defaultCover.png';
  }

  getImageContainerStyle(): { [key: string]: string } {
    if (this.cardSize === 'w-80') {
      return { 'padding-top': '80%' };
    } else if (this.cardSize === 'w-64') {
      return { 'padding-top': '64%' };
    } else {
      // Agregar más condiciones para otros tamaños de tarjetas
      return {};
    }
  }
}
