import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable()
export class FavSongInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const isLocal = (request.url.substring(0, 1) === '/');
    if (isLocal) {
      const token = this.tokenService.getToken();
      if (token != null) {
        const intReq = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        return next.handle(intReq);
      }
    }
    return next.handle(request);
  }
}
