export class FavSongDto {
  id!: number
  songUrl!: string;
  songName!: string;
  artistName!: string;
  albumCoverUrl?: string;
  albumName?: string;

  constructor(id: number, songUrl: string, songName: string, artistName: string, albumCoverUrl?: string, albumName?: string) {
    this.id = id;
    this.songUrl = songUrl;
    this.songName = songName;
    this.artistName = artistName;
    this.albumCoverUrl = albumCoverUrl;
    this.albumName = albumName;
  }
}
