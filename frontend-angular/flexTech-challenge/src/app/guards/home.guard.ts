import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  realRol!: string;
  constructor(private tokenSerivce: TokenService, private router: Router) { }


  canActivate(
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const expectedRoles = route.data['expectedRoles'];
    this.realRol = this.tokenSerivce.isAdmin() ? 'admin' : 'user';

    if (!this.tokenSerivce.isLogged() || expectedRoles.indexOf(this.realRol) < 0) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }


}
