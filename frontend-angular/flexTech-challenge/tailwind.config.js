/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#3A0CA3',
        'secondary': '#151528',
        'tertiary': '#CACAE3',
      },
    },
  },
  plugins: [],
}

